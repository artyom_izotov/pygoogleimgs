import os
import sys
import json
import time
import urllib2
from selenium import webdriver
from bs4 import BeautifulSoup as bs

def load_img( url, filename ):
  resource = urllib2.urlopen( url )
  output = open( filename, 'wb' )
  output.write( resource.read() )
  output.close()

def req( request ):
  r = request.replace( ' ', '%20' )
  return 'https://www.google.com.ua/search?q='+r+'&biw=1535&bih=791&source=lnms&tbm=isch&sa=X'

qs = ['heels', 'clogs and mules']
driver = webdriver.Firefox()
img_urls = {}
for q in qs:
  time.sleep( 1 )
  driver.get( req( q ) )
  html = driver.page_source
  soup = bs( html, 'lxml' )
  for a in soup.find_all( 'a', class_='rg_l' ):
    try:
      href = a['href']
      href = urllib2.unquote( href )
      href = href.replace( '/imgres?imgurl=', '' )
      href = href.split( '.jpg' )[0] + '.jpg'
    except:
      print 'failed to parse : ' + str(a)
    if q not in img_urls:
      img_urls[ q ] = []
    img_urls[ q ].append( href )
driver.quit()

def mkdir( dir ):
  if not os.path.exists( os.path.dirname( dir ) ):
      os.makedirs(os.path.dirname( dir ) )
  print dir

out_dir = 'imgs'
dirs = []
img_cnt = {}
for q in img_urls:
  dirs.append( dir )
  dir = out_dir + '/' + q + '/'
  mkdir( dir )
  if q not in img_cnt:
    img_cnt[ q ] = 0
  for u in img_urls[ q ]:
    try:
      load_img( u, dir + str( img_cnt[ q ] ) + '.jpg' )
      img_cnt[q] += 1
    except:
      print 'failed to load : ' + u
  print 'images for ' + q + ' done. Total : ' + str( img_cnt[ q ] )

print 'done'

